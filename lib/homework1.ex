defmodule Homework1 do
  @moduledoc """
  Documentation for `Homework1`.
  """

  # def my_flatten(iterable) do
  # end

  def is_palindrom?(input) do
    fixed_input = String.replace(input, " ","")
    |> String.downcase
    fixed_input == String.reverse(fixed_input)
  end
end
